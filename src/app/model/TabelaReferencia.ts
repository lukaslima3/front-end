export interface TabelaReferencia {
    id: number,
    codigo: number,
    mesAno: string,
    ano: number,
    mes: string
  }