import { Injectable } from '@angular/core';
import { Hero } from '../model/Hero';
import { TabelaReferencia } from '../model/TabelaReferencia';
import { HEROES } from '../model/mock-heroes';
import { Observable, of } from 'rxjs';
import { MessageService } from '../message.service';

import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root',
})
export class HeroService {
  private consultarTabelaDeReferencia = 'http://localhost:8090/consultarTabelaDeReferencia'
  constructor(private messageService: MessageService
    , private http: HttpClient
    ) { }


  getHeroes(): Observable<TabelaReferencia[]> {
    const heroes = of(HEROES);
    this.messageService.add('HeroService: fetched heroes');
    
    return this.http.get<any[]>(this.consultarTabelaDeReferencia);
  }

}