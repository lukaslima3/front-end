import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ListarMesReferenciaComponent } from './listar-mes-referencia/listar-mes-referencia.component';


const routes: Routes = [
  { path: 'heroes', component: ListarMesReferenciaComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
