import { Component, OnInit } from '@angular/core';
import { HeroService } from '../services/hero.service';
import { Hero } from '../model/Hero';
import { TabelaReferencia } from '../model/TabelaReferencia';
import { HEROES } from '../model/mock-heroes';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-listar-mes-referencia',
  templateUrl: './listar-mes-referencia.component.html',
  styleUrls: ['./listar-mes-referencia.component.css'],
  providers:  [ HeroService ]
})
export class ListarMesReferenciaComponent implements OnInit {
  heroes: Hero[] = [];
  arrayTabelaReferencia: TabelaReferencia[] = [];

  
  
  hero: Hero = {
    id: 1,
    name: 'Windstorm'
  };
  selectedHero: Hero | undefined;
  selectedTabelaReferencia: TabelaReferencia | undefined;
  constructor(private heroService: HeroService,
    private messageService: MessageService
    ) { }

  ngOnInit(): void {
    this.getHeroes();
  }

  selectHero(hero: Hero) { 
    this.messageService.add(`HeroesComponent: Selected hero id=${hero.id}`);
    this.selectedHero = hero; }

    getHeroes(): void {
      this.heroService.getHeroes()
      .subscribe(tabelaReferencia => {
        console.log(tabelaReferencia[0])
        this.arrayTabelaReferencia = tabelaReferencia;
      }
        
        );
    }

}
