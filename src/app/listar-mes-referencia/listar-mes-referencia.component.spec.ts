import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarMesReferenciaComponent } from './listar-mes-referencia.component';

describe('ListarMesReferenciaComponent', () => {
  let component: ListarMesReferenciaComponent;
  let fixture: ComponentFixture<ListarMesReferenciaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarMesReferenciaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarMesReferenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
